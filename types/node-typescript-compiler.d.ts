interface CompileOptions
{
  verbose?: boolean
  banner?: string
}

export function compile(
  tscOptions?: object,
  files?: string[],
  options?: CompileOptions
): Promise<string>
